#!/bin/bash

# Heroku dyno is missing Mysql client. Try
# restore via Travis post deploy step

if [ -f dbdump/.mysqldump.sql ]; then
####  cd public.built
####  export WP_CLI_CONFIG_PATH="$(pwd)/public.built"
####  wp db import --allow-root dbdump/.mysqldump.sql

  # Replace K8s dev hostname, and rely on relative URL
  sed -i "s/http[s:\/]+$K8SHOST//g" dbdump/.mysqldump.sql

  # Recreate database
  mysql --user=$DBUSERNAME --password=$DBPASSWORD --database=$DBDATABASE --host=$DBHOST < dbdump/.mysqldump.sql

  # Change site and home URL to the Heroku staging deployment
  mysql --user=$DBUSERNAME --password=$DBPASSWORD --database=$DBDATABASE --host=$DBHOST \
        -e "UPDATE wp_options SET option_value='http://$HEROKUAPP.herokuapp.com' WHERE option_name='siteurl' OR option_name='home';"

fi


