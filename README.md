Wordpress Staging on Heroku [![Build Status](https://travis-ci.org/patterns/wpmirror.svg?branch=master)](https://travis-ci.org/patterns/wpmirror)
=========

* A dev sandbox gets changed through Wordpress Admin UI.
* The changes are version tracked with commits to this repo.
* These files are then pushed to deploy [WordPress](http://wordpress.org/) on [Heroku](http://www.heroku.com/) 


Notes
------------

* Travis CI calls phpunit by default. Starting with a placeholder unit test, for now.
* WP-CLI is called to export the db, but mysqldump direct call also worked fine in experiments.
* Heroku dyno is missing Mysql client tools. To restore the db, try with Travis post deploy step. (The Jaws-MariaDB doc describes mysql import.)
* Heroku CLI is available with npm install -g heroku-cli. However calling heroku config:get WP_DB_URL requires credentials
* encrypt mysql login into Travis env vars. Travis settings allows this via web UI.
* 1046 No database selected. Setting --database in mysql cli call
* 1044 Access denied for user to database 'wordpress'. Choose --database to be the garbled name (see stackoverflow.com/a/37240757)
* Not obvious whether db restore worked. Update siteurl and home in wp_options table is not enough.
* Search for old url and replace with new url. Finally! sed -i "s/http[s:\/]+$K8SHOST//g"  [(see hint about post_content table)](https://blog.wsol.com/getting-to-know-your-wordpress-database/) 
* Also detached heroku add-ons (redis, new relic) to eliminate any caching during the replace debugging.



Credits
------------

[Heroku WP by Xiao Yu](https://github.com/xyu/heroku-wp.git)
   [(LICENSE)](https://github.com/xyu/heroku-wp/blob/nginx-php7/LICENSE)


